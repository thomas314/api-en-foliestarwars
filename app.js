

// Characters
$.getJSON("./packages/api-starwars.json", function(data) {
  for (var i = 0; i < data.results.length; i++) {
    var character = data.results[i];
    var tplt ="<option class='char-option' data-value='{{character}}'>{{name}}</option>";

    var output = Mustache.render(tplt, character);
    $("#character").append(output);
    console.log(output);
  }

  $("#character").on("change", function(){

	console.log( $(this).find(":selected").data('value') );

	var selecteur = $(this).find(":selected").data('value');

  var character_info = {
    name: selecteur.name,
    height: selecteur.height,
    weight: selecteur.mass,
    hair_color: selecteur.hair_color,
    skin_color: selecteur.skin_color,
    eye_color: selecteur.eye_color,
    birth_year: selecteur.birth_year,
    gender: selecteur.gender,
    homeworld:selecteur.homeworld,
    films: selecteur.films,
    species: selecteur.species,
    vehicles: selecteur.vehicles,
    starships: user_select.starships,
    created: user_select.created,
    edited: user_select.edited,
    url: user_select.url
  };

  var template_chara =
    "<h2>name: {{name}}</h2>" +
    "<p>height: {{height}}</p>" +
    "<p>weight: {{weight}}</p>" +
    "<p>hair color: {{hair_color}}</p>" +
    "<p>skin color: {{skin_color}}</p>" +
    "<p>eye color: {{eye_color}}</p>" +
    "<p>year of birth: {{birth_year}}</p>" +
    "<p>gender: {{gender}}</p>" +
    "<p>homeworld: {{homeworld}}</p>" +
    "<p>films: {{films}}</p>" +
    "<p>species: {{species}}</p>" +
    "<p>vehicles: {{vehicles}}</p>" +
    "<p>starships: {{starships}}</p>" +
    "<p>profil created on: {{created}}</p>" +
    "<p>profil edited on: {{edited}}</p>" +
    "<p>url: {{url}}</p>";

  var info_chara = Mustache.render(template_chara, character_info);
  $("#inform_u_character").append(info_chara);
});
})
// Movies
$.getJSON("./packages/api-starwars-movie.json", function(data) {
  for (var i = 0; i < data.results.length; i++) {
    var movie = data.results[i];
    var tplt ="<option class='movie-option' data-value='{{movie}}'>{{title}}</option>";

    var output = Mustache.render(tplt, movie);
    $("#movie").append(output);
    console.log(output);
  }



  $("#movie").on("change", function(){

	console.log( $(this).find(":selected").data('value') );

	var user_select = $(this).find(":selected").data('value');


  var movie_info = {
    title: user_select.title,
    episode_id: user_select.episode_id,
    opening_crawl: user_select.opening_crawl,
    director: user_select.director,
    producer: user_select.producer,
    release_date: user_select.release_date,
    characters: user_select.characters,
    planets: user_select.planets,
    starships: user_select.starships,
    vehicles: user_select.vehicles,
    species: user_select.species,
    created: user_select.created,
    edited: user_select.edited,
    url: user_select.url
  };

  var template_movie =
    "<h2>title: {{title}}</h2>" +
    "<p>episode_id: {{episode_id}}</p>" +
    "<p>opening_crawl: {{opening_crawl}}</p>" +
    "<p>director: {{director}}</p>" +
    "<p>producer: {{producer}}</p>" +
    "<p>release_date: {{release_date}}</p>" +
    "<p>characters: {{characters}}</p>" +
    "<p>planets: {{planets}}</p>" +
    "<p>starships: {{starships}}</p>" +
    "<p>vehicles: {{vehicles}}</p>" +
    "<p>species: {{species}}</p>" +
    "<p>profil created on: {{created}}</p>" +
    "<p>profil edited on: {{edited}}</p>" +
    "<p>url: {{url}}</p>";

  var info_movie = Mustache.render(template_movie, movie_info);
  $("#inform_u_movie").append(info_movie);
});
})
// PLanets
$.getJSON("./packages/api-starwars-planet.json", function(data) {
  for (var i = 0; i < data.results.length; i++) {
    var planet = data.results[i];
    var tplt =
    "<option class='char-option' data-value='{{planet}}'>{{name}}</option>";
    
    var output = Mustache.render(tplt, planet);
    $("#planet").append(output);
    console.log(output);
}
})
$("#planet").on("change", function(){

	console.log( $(this).find(":selected").data('value') );

	var user_select = $(this).find(":selected").data('value');

})
  var planet_info = {
    name: user_select.name,
    rotation_period: user_select.rotation_period,
    orbital_period: user_select.orbital_period,
    diameter: user_select.diameter,
    climate: user_select.climate,
    gravity: user_select.gravity,
    terrain: user_select.terrain,
    surface_water: user_select.surface_water,
    population: user_select.population,
    residents: user_select.residents,
    films: user_select.films,
    created: user_select.created,
    edited: user_select.edited,
    url: user_select.url
  };

  var template_planet =
    "<h2>name: {{name}}</h2>" +
    "<p>rotation_period: {{rotation_period}}</p>" +
    "<p>orbital_period: {{orbital_period}}</p>" +
    "<p>diameter: {{diameter}}</p>" +
    "<p>climate: {{climate}}</p>" +
    "<p>gravity: {{gravity}}</p>" +
    "<p>terrain: {{terrain}}</p>" +
    "<p>surface_water: {{surface_water}}</p>" +
    "<p>population: {{population}}</p>" +
    "<p>residents: {{residents}}</p>" +
    "<p>films: {{films}}</p>" +
    "<p>profil created on: {{created}}</p>" +
    "<p>profil edited on: {{edited}}</p>" +
    "<p>url: {{url}}</p>";

  var info_planet = Mustache.render(template_planet, planet_info);
  $("#inform_u_planet").append(info_planet);

